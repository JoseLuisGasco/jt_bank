var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitecujlgg/collections/";
var mLabAPIKey = "apiKey=E7qNG698tg90u0NLfeSbApQJH5mWRJmz";
var requestJson = require('request-json');

var openWeatherURL = "http://api.openweathermap.org/data/2.5/";
var openWAPIKey = "APPID=91f0d46db8d9becc5116ff09aefdd4a8";
var openWunits = "units=metric";
var openWLenguaje = "lang=es";

const enigma = require('enigma-code');
const valorEncriptacion = 10;//puede ser cualquier numero
let key = 'jose2018';
let mypassword = 'jose12345678';

app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");

    res.send(
      {
        "msg" : "Bienvenido a la API de Tech University"
      }
    )
  }
);

// Consulta de datos un cliente //

//app.get('/jtbank/v1/clientes/:idcliente/',
app.get('/jtbank/v1/clientes/datoscliente/',
  function(req, res) {
    console.log("GET /apitechu/v1/clientes/datoscliente");

    //var idcliente = req.params.idcliente;
    var idcliente = req.headers.idcliente;
    var query = 'q={"idcliente" : ' + idcliente + '}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo cliente."
          };
          res.status(500);
        } else {
            if (body.length > 0) {
              response = body[0];
            } else {
                response = {
                  "msg" : "Cliente no encontrado."
                };
                res.status(404);
              }
          };
        res.send(response);
      }
    )
  }
);

// Login de un cliente //
// Se recuperan los datos del cliente//
// Se compara la password recuperada de BBDD y //
// la password indicada por el cliente //

app.post("/jtbank/v1/login",
  function(req, res) {

    var email = req.body.email;
    var password = req.body.password;

    var query = 'q={"email":"' + email + '"}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(errLoggin, resMLabLoggin, bodyLoggin) {
        if (errLoggin) {
          response = {
            "msg" : "Error Loggin"
          }
          res.status(500);
          res.send(response);
        } else {
          if (bodyLoggin.length > 0) {

            let hash = bodyLoggin[0].password;

            enigma.comparar(hash,password, function(err, rescom) {
		            if(err) {
                  response ={
                    "msg":"Error Loggin"
                  };
                  res.status(500);
                  res.send(response);
                } else {

                    if (rescom) {

                      var queryPut='q={"idcliente":'+bodyLoggin[0].idcliente+'}';
                      var putBody ='{"$set":{"logged":true}}';
                      response = bodyLoggin[0];

                      httpClient.put("clientes?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
                        function(errPut,resMlabPut,bodyPut){
                          console.log('ERROR PUT:'+errPut);
                          console.log('BODY PUT:'+bodyPut);
                          res.send(response);
                        }
                      );
                    } else {
                        response = {
                          "msg" : "Password no valida"
                        };
                        res.status(500);
                        res.send(response);
                      }
                  }
              }
            )
          } else {
              response = {
                "msg" : "Cliente no encontrado."
              };
              res.status(404);
              res.send(response);
            }
        }
      }
    )
  }
);

// Logout de un cliente //

app.post("/jtbank/v1/logout",
  function(req, res) {

    var email = req.body.email;
    var query = 'q={"email":"' + email + '", "logged":true}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo Cliente."
          }
          res.status(500);
          res.send(response);

        } else {
          if (body.length > 0) {

            var queryPut='q={"idcliente":'+body[0].idcliente+'}';
            var putBody ='{"$unset":{"logged":""}}';
            //response = body[0];
            response = null;

            httpClient.put("clientes?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,bodyPut){
                console.log('ERROR PUT:'+errPut);
                console.log('BODY PUT:'+bodyPut);
                res.send(response);
              }
            )
          } else {
              response = {
                "msg" : "Cliente no encontrado."
              };
              res.status(404);
              res.send(response);
            }
        }
      }
    )
  }
);

// Alta de un nuevo cliente //
// Se valida que el cliente no existe //
// Se asigna un nuevo ID, consecutivo al mayor de la  BBDD//
// Se encripta la password y se da de alta el nuevo cliente //

app.post("/jtbank/v1/altacliente",
  function(req, res) {

    var emailAltaCliente = req.body.email;
    var passwordAltaCliente = req.body.password;
    var nombreAltaCliente = req.body.nombre;
    var apellidosAltaCliente = req.body.apellidos;
    var direccionAltaCliente = req.body.direccion;
    var ciudadAltaCliente = req.body.ciudad;
    var paisAltaCliente = req.body.pais;
    var telefonoAltaCliente = req.body.telefono;

    var querysort='s={"idcliente":-1}';
    var queryvalidate='q={"email":"' + emailAltaCliente + '"}';
    var querymaxid='q={}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?"+ queryvalidate + "&" + mLabAPIKey,
      function(errvuser, resMLabvuser, bodyvuser) {
        if (errvuser) {
          response = {
            "msg" : "Error Alta Cliente"
          };
          res.status(500);
          res.send(response);

        } else {
            if (bodyvuser.length > 0) {
              response = {
                "msg" : "Error, el Cliente existe"
              }
              res.status(500);
              res.send(response);
            } else {

                httpClient.get("clientes?"+ querymaxid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
                  function(errmaxid, resMLabmaxid, bodymaxid) {

                    if (errmaxid) {
                      response = {
                        "msg" : "Error Alta Cliente"
                      };
                      res.status(500);
                      res.send(response);

                    } else {

                        if (bodymaxid.length>0) {

                          var newIdCliente = bodymaxid[0].idcliente + 1;

                          var passwordEncriptada = enigma.genHash(valorEncriptacion,key,passwordAltaCliente,function(err,hash){
                                                    if(err) return console.log(err);
                                                    return(hash);
                          });

                          var insertBody = '{"idcliente": '+ newIdCliente +' , "nombre":"' + nombreAltaCliente + '", "apellidos":"' + apellidosAltaCliente + '", "email":"' + emailAltaCliente + '", "direccion":"' + direccionAltaCliente + '", "ciudad":"' + ciudadAltaCliente + '", "pais":"' + paisAltaCliente + '", "telefono":"' + telefonoAltaCliente + '", "password":"' + passwordEncriptada + '", "logged":true}';

                          httpClient.post("clientes?" + mLabAPIKey, JSON.parse(insertBody),
                            function(errPost, resMLabPost, bodyPost) {
                              response = insertBody;
                              res.send(response);
                            }
                          )
                        } else {
                            response = {
                              "msg" : "Error Alta Cliente"
                            };
                            res.status(500);
                            res.send(response);
                          }
                      }
                  }
                )
              }
          }
      }
    )
  }
);

// Se declaran las funciones de encriptado y comparación de claves //

enigma.genHash(valorEncriptacion, key, mypassword, function(err, hash){
	if(err) return console.log(err);
	console.log('hash', hash);

	enigma.comparar(hash, mypassword, function(err, response){
		if(err) return console.log(err);
		if(response) return console.log("Test passed");
		return console.error("Test not passed");
	})
});

// Modificacion datos un cliente //

app.post("/jtbank/v1/cliente/modcliente/",
  function(req, res) {

    var idcliente = req.body.idcliente;
    var nombre = req.body.nombre;
    var apellidos = req.body.apellidos;
    var direccion = req.body.direccion;
    var ciudad = req.body.ciudad;
    var pais = req.body.pais;
    var telefono = req.body.telefono;

    var query = 'q={"idcliente":' + idcliente + '}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(err, resMLab, bodyget) {
        if (err) {
          response = {
            "msg" : "Error obteniendo Cliente."
          }
          res.status(500);
          res.send(response);

        } else {
          if (bodyget.length > 0) {

            var queryPut='q={"idcliente":'+bodyget[0].idcliente+'}';
            var putBody ='{"$set":{"nombre":"' + nombre + '", "apellidos":"' + apellidos + '", "direccion":"' + direccion + '", "ciudad":"' + ciudad + '", "pais":"' + pais + '", "telefono":"' + telefono + '"}}';

            httpClient.put("clientes?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,bodyPut){
                console.log('ERROR PUT:'+errPut);
                console.log('BODY PUT:'+bodyPut);
                response = bodyPut;
                res.send(response);
              }
            )
          } else {
              response = {
                "msg" : "Cliente no encontrado."
              };
              res.status(404);
              res.send(response);
            }
        }
      }
    )
  }
);

// Consulta de las notificaciones de un cliente
// Se recive el id del cliente y se recuperan todas sus notificaciones

//app.get('/jtbank/v1/cliente/:idcliente/notificaciones',
app.get('/jtbank/v1/cliente/notificaciones',
  function(req, res) {

    //var idcliente = req.params.idcliente;
    var idcliente = req.headers.idcliente;
    var query = 'q={"idcliente" : ' + idcliente + '}';
    var querysort='s={"notificacionid":-1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("notificaciones?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo Notificaciones"
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              response = body;
            } else {
                response = {
                  "msg" : "No hay Notificaciones"
                };
                res.status(404);
              }
          }
        res.send(response);
      }
    )
  }
);

// Modificacion status Recibido de una Notificacion //

app.post("/jtbank/v1/clientes/editnotificacion/",
  function(req, res) {

    var notificacionid = req.body.notificacionid;
    var query = 'q={"notificacionid":' + notificacionid + '}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("notificaciones?" + query + "&" + mLabAPIKey,
      function(err, resMLab, bodyget) {
        if (err) {
          response = {
            "msg" : "Error obteniendo Notificacion"
          }
          res.status(500);
          res.send(response);

        } else {
          if (bodyget.length > 0) {

            var queryPut='q={"notificacionid":'+bodyget[0].notificacionid+'}';
            var putBody ='{"$set":{"leido":true}}';

            httpClient.put("notificaciones?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,bodyPut){

                response = bodyPut;
                res.send(response);
              }
            )
          } else {
              response = {
                "msg" : "Notificacion no encontrada."
              };
              res.status(404);
              res.send(response);
            }
        }
      }
    )
  }
);


// Consulta de las cuentas de un cliente
// Se recive el id del cliente y se recuperan todas sus cuentas

//app.get('/jtbank/v1/cliente/:idcliente/cuentas',
app.get('/jtbank/v1/cliente/cuentas',
  function(req, res) {

    //var idcliente = req.params.idcliente;
    var idcliente = req.headers.idcliente;
    var query = 'q={"idcliente" : ' + idcliente + '}';
    var querysort='s={"iban":1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo IBAN."
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              response = body;
            } else {
                response = {
                  "msg" : "IBAN no encontrado."
                };
                res.status(404);
              }
          }
        res.send(response);
      }
    )
  }
);

// Crear una cuentas de un cliente
// Se recupera el max.id de cuentas
// Componemos el iban en funcion de fecha y hora
// POST de la nueva cuentas

app.post('/jtbank/v1/clientes/altacuenta/',
  function(req, res) {

    var querymaxcuentaid='q={}';
    var querysort = 's={"cuentaid":-1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ querymaxcuentaid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
      function(errmaxcuentaid, resMLabmaxcuentaid, bodymaxcuentaid) {

        if (errmaxcuentaid) {
          response = {
            "msg" : "Error insertando cuenta."
          };
          res.status(500);
          res.send(response);
        } else {
            if (bodymaxcuentaid.length>0) {

              var idcliente = req.body.idcliente;

              var dateAltaCuenta = new Date();
              var dAltaCuenta = dateAltaCuenta.getDate();
              var diaAltaCuenta = (dAltaCuenta < 10) ? '0' + dAltaCuenta : dAltaCuenta;
              var mAltaCuenta = dateAltaCuenta.getMonth() + 1;
              var mesAltaCuenta = (mAltaCuenta < 10) ? '0' + mAltaCuenta : mAltaCuenta;
              var hAltaCuenta = dateAltaCuenta.getHours();
              var horaAltacuenta = (hAltaCuenta < 10) ? '0' + hAltaCuenta : hAltaCuenta;
              var mtoAltaCuenta = dateAltaCuenta.getMinutes();
              var minutoAltaCuenta = (mtoAltaCuenta < 10) ? '0' + mtoAltaCuenta : mtoAltaCuenta;

              var newiban = "BA01 0001 7999 " + dateAltaCuenta.getFullYear() + " " + mesAltaCuenta + diaAltaCuenta + " " + horaAltacuenta + minutoAltaCuenta;

              var newCuentaId = bodymaxcuentaid[0].cuentaid + 1;
              var insertCuenta = '{"iban":"' + newiban + '", "cuentaid":' + newCuentaId + ', "idcliente":' + idcliente + ', "saldo":0}';

              httpClient.post("cuentas?" + mLabAPIKey, JSON.parse(insertCuenta),
                function(errPostCuenta, resMLabPostCuenta, bodyPostCuenta) {

                  response = insertCuenta;
                  res.send(response);
                }
              )
            }
          }
      }
    )
  }
);

// Consulta de los movimientos de una cuenta de un cliente


//app.get('/jtbank/v1/cuenta/:cuentaid/movimientos',
app.get('/jtbank/v1/cuenta/movimientos',
  function(req, res) {

    //var cuentaid = req.params.cuentaid;
    var cuentaid = req.headers.cuentaid;
    var query = 'q={"cuentaid" : ' + cuentaid + '}';
    var querysort='s={"movimientoid":-1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("movimientos?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(errmov, resMLabmov, bodymov) {
        if (errmov) {
          response = {
            "msg" : "Error obteniendo IBAN."
          }
          res.status(500);
        } else {
            response = bodymov;
          }
        res.send(response);
      }
    )
  }
);

// Alta de movimientos de una cuenta de entrada
// Validamos que existe la cuenta indicada
// Recuperamos el max. id de movimientos
// Recuperamos fecha y hora del sistema
// POST del nuevo movimiento
// Actualizamos saldo de la cuenta
// Si la actualización del saldo de la cuenta da error eliminamos
// el movimiento que se había insertado

app.post("/jtbank/v1/cuenta/altamovimiento",
  function(req, res) {

    var cuentaid = req.body.cuentaid;
    var tipoAltaMov = req.body.tipo;
    var importeAltaMov = req.body.importe;
    var observacionesAltaMov = req.body.observaciones;

    var dateAltaMov = new Date();
    var dAltaMov = dateAltaMov.getDate();
    var diaAltaMov = (dAltaMov < 10) ? '0' + dAltaMov : dAltaMov;
    var mAltaMov = dateAltaMov.getMonth() + 1;
    var mesAltaMov = (mAltaMov < 10) ? '0' + mAltaMov : mAltaMov;
    var hAltaMov = dateAltaMov.getHours();
    var hAltaMov = (hAltaMov < 10) ? '0' + hAltaMov : hAltaMov;
    var mAltaMov = dateAltaMov.getMinutes();
    var minutoAltaMov = (mAltaMov < 10) ? '0' + mAltaMov : mAltaMov;
    var fechaAltaMov = diaAltaMov + "/" + mesAltaMov + "/" + dateAltaMov.getFullYear();
    var horaAltaMov = hAltaMov + ":" + minutoAltaMov;

    var querysort='s={"movimientoid":-1}';
    var queryvalidate='q={"cuentaid":' + cuentaid + '}';
    var querymaxid='q={}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ queryvalidate + "&" + mLabAPIKey,
      function(errvcuenta, resMLabvcuenta, bodyvcuenta) {
        if (errvcuenta) {
            response = {
              "msg" : "Error Alta Movimiento"
            };
            res.status(500);
            res.send(response);
        } else {
            if (bodyvcuenta.length > 0) {

              httpClient.get("movimientos?"+ querymaxid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
                function(errmaxid, resMLabmaxid, bodymaxid) {

                  if (errmaxid) {
                    response = {
                      "msg" : "Error Alta Movimiento"
                    };
                    res.status(500);
                    res.send(response);
                  } else {

                      if (bodymaxid.length>0) {
                        var newMovimientoId = bodymaxid[0].movimientoid + 1;

                        if (tipoAltaMov === "Ingreso de Efectivo" || tipoAltaMov === "Ingreso Transferencia" || tipoAltaMov === "Ingreso de Nomina") {
                          var saldoposAltaMov = bodyvcuenta[0].saldo + parseFloat(importeAltaMov);
                        } else {
                          var saldoposAltaMov = bodyvcuenta[0].saldo - importeAltaMov
                          };

                        var saldoantAltaMov = bodyvcuenta[0].saldo;

                        var insertBodyMov = '{"cuentaid": ' + cuentaid + ', "movimientoid": ' + newMovimientoId + ', "importe":' + importeAltaMov + ', "saldoant":' + saldoantAltaMov + ', "saldopos":' + saldoposAltaMov + ', "tipo":"' + tipoAltaMov + '", "fecha":"' + fechaAltaMov + '", "hora":"' + horaAltaMov + '", "observaciones":"' + observacionesAltaMov + '"}';

                        httpClient.post("movimientos?" + mLabAPIKey, JSON.parse(insertBodyMov),
                          function(errPostMov, resMLabPostMov, bodyPostMov) {

                            if (errPostMov) {
                              response =  {
                                "msg":"Error Alta Movimiento"
                              };
                              res.status(500);
                              res.send(response);
                            } else {

                  // Actualizar saldo cuenta

                              var queryPutCuentas='q={"cuentaid":'+bodyvcuenta[0].cuentaid+'}';
                              var putBodyCuentas ='{"$set":{"saldo":'+saldoposAltaMov+'}}';

                              httpClient.put("cuentas?"+queryPutCuentas+'&'+mLabAPIKey, JSON.parse(putBodyCuentas),
                                function(errPutCuentas,resMlabPutCuentas,bodyPutCuentas){
                                  if (errPutCuentas) {

                  //Si la actualizacon del saldo de la cuenta da error: Borramos el Movimiento

                                    var queryPutMovimientos='q={"movimientosid":'+newMovimientoId+'}';
                                    var putBodyMovimientos ='{"$set":{}}';

                                    httpClient.put("movimientos?"+queryPutMovimientos+'&'+mLabAPIKey, JSON.parse(putBodyMovimientos),
                                      function(errPutMovimientos,resMlabPutMovimientos,bodyPutMovimientos){
                                        response = {
                                          "msg":"Error Alta Movimiento"
                                        };
                                        res.status(500);
                                        res.send(Response);
                                      });
                                  } else {
                                      console.log('Finaliza proceso Alta Movimiento');

                                      response = insertBodyMov;
                                      res.send(response);
                                    }
                                }
                              );
                            }
                          }
                        )
                      } else {
                          response = {
                            "msg" : "Error Alta Movimiento"
                          };
                          res.status(500);
                          res.send(response);
                        }
                    }
                }
              )

            } else {
                response = {
                  "msg" : "Error Alta Movimiento"
                }
                res.status(500);
                res.send(response);
              }
          }
      }
    )
  }
);


// Llamada a la API Open Weather
// Llamamos para consultar la situacion metorologíca de la ciudad del cliente para
// el día actual y los dos próximos.

//app.get('/jtbank/v1/cliente/:ciudad/bienvenida/',
app.get('/jtbank/v1/cliente/bienvenida/',
  function(req, res) {

    //var ciudad = req.params.ciudad;
    var ciudad = req.headers.ciudad;
    var openWeatherURL = "http://api.openweathermap.org/data/2.5/";
    var openWAPIKey = "APPID=91f0d46db8d9becc5116ff09aefdd4a8";
    var openWunits = "units=metric";
    var openWlenguaje = "lang=es";
    var cnt = "cnt=4";

    httpClient = requestJson.createClient(openWeatherURL);

    httpClient.get("forecast?q=" + ciudad + "&" + openWAPIKey + "&" + cnt + "&" + openWunits + "&" + openWlenguaje,
      function(errWeather, resWeather, bodyWeather) {

        if (errWeather) {
          response = {
            "msg" : "Error obteniendo Pred. Meteor."
          }
          res.status(500);
        } else {
            var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
            var datePrevMet = new Date();
            var hoy = datePrevMet.getDay();
            var dDos = hoy + 1;
            var dTres = hoy + 2;
            var dCuatro = hoy + 3;
            var diaHoy = diasSemana[hoy];
            var diaDos = diasSemana[dDos];
            var diaTres = diasSemana[dTres];
            var diaCuatro = diasSemana[dCuatro];

            var i;
            for (i = 0; i < 4; i ++) {
              var condicionIcono = bodyWeather.list[i].weather[0].icon;
              var icon;

              switch( condicionIcono ){
                case "01d": case "01n":
                  icon = "wi-day-sunny";
                  break;
                case "02d": case "02n":
                  icon = "wi-day-cloudy";
                  break;
                case "03d": case "03n":
                case "04d": case "04n":
                  icon = "wi-cloudy";
                  break;
                case "09d": case "09n":
                  icon = "wi-rain";
                  break;
                case "10d": case "10n":
                  icon = "wi-day-rain-mix";
                  break;
                case "11d": case "11n":
                  icon = "wi-thunderstorm";
                  break;
                case "13d": case "13n":
                  icon = "wi-snow";
                  break;
                case "50d": case "50n":
                  icon = "wi-fog";
                  break;
                default:
                  icon = "wi-day-sunny";
                  break;
              };

              if (i===0) {
                var iconoHoy = icon;
                var temperaturaHoy = Math.round(bodyWeather.list[i].main.temp);
                var humedadHoy = Math.round(bodyWeather.list[i].main.humidity);
                var descripcionHoy = bodyWeather.list[i].weather[0].description;
              } else {
                  if (i===1) {
                    var iconoDos = icon;
                    var descripcionDos = bodyWeather.list[i].weather[0].description;
                  } else {
                      if (i===2) {
                        var iconoTres = icon;
                        var descripcionTres = bodyWeather.list[i].weather[0].description;
                      } else {
                        var iconoCuatro = icon;
                        var descripcionCuatro = bodyWeather.list[i].weather[0].description;
                        }
                    }
                };
            };

            var responseWeather = '{"diaHoy": "' + diaHoy + '", "temperaturaHoy": "' + temperaturaHoy + '", "humedadHoy": "' + humedadHoy + '", "descripcionHoy": "' + descripcionHoy + '", "iconoHoy": "' + iconoHoy + '", "diaDos": "' + diaDos + '", "iconoDos": "' + iconoDos + '", "descripcionDos": "' + descripcionDos + '", "diaTres": "' + diaTres + '", "iconoTres": "' + iconoTres + '", "descripcionTres": "' + descripcionTres + '", "diaCuatro": "' + diaCuatro + '", "iconoCuatro": "' + iconoCuatro + '", "descripcionCuatro": "' + descripcionCuatro + '", "latitud": ' + bodyWeather.city.coord.lat + ', "longitud": ' + bodyWeather.city.coord.lon + '}';

            response = responseWeather;
          };
        res.send(response);
      }
    )
  }
);
